<?php

function add($booking_date, $hour, $nom, $prenom, $mail, $tel)
{
    global $bdd;

    $reqSQL='
        INSERT INTO booking (booking_date, hour, nom_client, prenom_client, email, telephone)
        VALUES(:booking_date, :hour, :nom, :prenom, :mail, :tel)
    ';

    $requete = $bdd->prepare($reqSQL);
    $requete->bindValue(':booking_date', $booking_date);
    $requete->bindValue(':hour', $hour);
    $requete->bindValue(':nom', $nom);
    $requete->bindValue(':prenom', $prenom);
    $requete->bindValue(':mail', $mail);
    $requete->bindValue(':tel', $tel);
    $requete->execute();
    $requete->closeCursor();
}


function selectAll() {
    global $bdd;
    $reqSQL='SELECT * FROM booking';
    $requete=$bdd->query($reqSQL);
    $bookings = $requete->fetchAll();
    $requete->closeCursor();
    return $bookings;
}

function selectDate($date) {
    global $bdd;
    $reqSQL= $bdd->prepare('SELECT * FROM booking WHERE booking_date like :bd');
    $reqSQL->bindValue(':bd', $date, PDO::PARAM_STR);
    $reqSQL->execute();
    $bookings = $reqSQL->fetchAll();
    return $bookings;
}

function selectHour($date) {
    global $bdd;
    $reqSQL= $bdd->prepare('SELECT hour FROM booking WHERE booking_date like :bd');
    $reqSQL->bindValue(':bd', $date, PDO::PARAM_STR);
    $reqSQL->execute();
    $bookings = $reqSQL->fetchAll();
    return $bookings;
}

function MODELE_hours() {
    $temp_hours = file_get_contents ('hours.json');
    $temp_hours = json_decode($temp_hours);
    return $temp_hours;

}


