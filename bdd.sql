DROP DATABASE IF EXISTS `booking_coiffure`;

CREATE DATABASE `booking_coiffure`;

USE `booking_coiffure`;

CREATE TABLE `booking` (
  `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `booking_date` date  NOT NULL,
  `hour` INT (5) NOT NULL,
  `nom_client` varchar(255) NOT NULL,
  `prenom_client` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` varchar(15) NOT NULL, 
  UNIQUE (booking_date, hour)
  );


-- CREATE TABLE `hour_week` (
--   `id_hour_week` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
--   `hour` varchar(255) NULL,
--   `dispo` int NULL
-- );

-- CREATE TABLE `hour_saturday` (
--   `id_hour_saturday` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
--   `hour` varchar(255) NULL,
--   `dispo` int NULL
-- );

-- INSERT INTO hour_week VALUES ("1","09:00", "1");
-- INSERT INTO hour_week VALUES ("2","10:00", "1");
-- INSERT INTO hour_week VALUES ("3","11:00", "1");
-- INSERT INTO hour_week VALUES ("4","13:00", "1");
-- INSERT INTO hour_week VALUES ("5","14:00", "1");
-- INSERT INTO hour_week VALUES ("6","15:00", "1");
-- INSERT INTO hour_week VALUES ("7","16:00", "1");
-- INSERT INTO hour_week VALUES ("8","17:00", "1");


-- INSERT INTO hour_saturday VALUES ("1","09:00", "1");
-- INSERT INTO hour_saturday VALUES ("2","10:00", "1");
-- INSERT INTO hour_saturday VALUES ("3","11:00", "1");
-- INSERT INTO hour_saturday VALUES ("4","12:00", "1");
-- INSERT INTO hour_saturday VALUES ("5","13:00", "1");
-- INSERT INTO hour_saturday VALUES ("6","14:00", "1");
-- INSERT INTO hour_saturday VALUES ("7","15:00", "1");
-- INSERT INTO hour_saturday VALUES ("8","16:00", "1");
-- INSERT INTO hour_saturday VALUES ("9","17:00", "1");
-- INSERT INTO hour_saturday VALUES ("10","18:00", "1");

-- CREATE TABLE `booking_date_week` (
--   `id_booking_date` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
--   ` booking_date` varchar(255) NULL,
--   `id_hour_week` INT NULL,
--   `id_hour_saturday` INT NULL,
--   FOREIGN KEY (`id_hour_week`) REFERENCES hour_week(`id_hour_week`),
--   FOREIGN KEY (`id_hour_saturday`) REFERENCES hour_saturday(`id_hour_saturday`)
-- );

