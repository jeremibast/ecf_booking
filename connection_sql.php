<?php
function connexionBD($dbname){
	// param�tres de la base de donn�e
	$sgbdname='mysql';
	$host='localhost';
	$charset='utf8';

	// dsn : data source name
	$dsn = $sgbdname.':host='.$host.';dbname='.$dbname.';charset='.$charset;

	// utilisateur connect� � la base de donn�e
	$username = 'admin';
	$password = 'adminadmin';

	// pour avoir des erreurs SQL plus claires
	$erreur = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

	try {
	    $bdd = new PDO($dsn, $username, $password, $erreur);
	} catch (PDOException $e) {
	    die ('Connection Failed : ' . $e->getMessage() );
	}
	return $bdd;
}
$bdd=connexionBD('booking_coiffure');