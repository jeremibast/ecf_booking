<?php
// on est dans l'index du site : au démaragge. Rien n'existe à part les super globales
// error_reporting(E_ALL);
// ini_set('display_errors', 'On');
// On demarre les sessions
session_start();


// Système de debug simple
$debug = 0;
function debug($variable)
{
    global $debug;
    if ($debug == 1) {
        echo '<pre>';
        print_r($variable);
        echo '</pre>';
    }
    if ($debug == 2) {
        print_r($variable);
    }
}
if ($debug == 1 or $debug == 2) {
    echo 'IndexSwitch.php - SESSION : ';
    print_r($_SESSION);
    echo '<br/>';
    echo 'POST : ';
    print_r($_POST);
    echo '<br/>';
    echo 'GET : ';
    print_r($_GET);
    echo '<br/>';
}
include_once('controller.php');
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <title>HAIR PODS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Appel de la Feuille de style minifiée De La librairie Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Appel de la Feuille de style minifiée De l'extension Datepicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <!-- Appel de la Bibliothèque Font Awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Appel de la police Montserrat et Open Sans -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:700|Open+Sans:300" rel="stylesheet">
    <!-- Feuille de style Personnalisée -->
    <link rel="stylesheet" href="style.css">
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">

                <h3> HAIR PODS </h3>

                <!-- Champ -->
                <form action="" method="post">
                    <div class="form-group">
                        <div class=" datepicker date input-group shadow-sm">
                            <input type="text" placeholder="Choisir une date" class="form-control py-4 px-4" id="reservationDate">
                            <div class="input-group-append"><span class="input-group-text px-4"><i class="fa fa-calendar"></i></span></div>
                            <input type="hidden" name="choix">
                            <button type="submit" class="btn btn-primary">Choisir</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <?php if (isset($_POST['choix'])) {
                $scheduled_day = $_POST['choix'];
                $days = ['0', '1', '2', '3', '4', '5', '6'];
                $day = date('w', strtotime($scheduled_day));
                $scheduled_day = date('y-m-d', strtotime($scheduled_day)) . " ($days[$day])";
                

                if ($days[$day] < 2) {
                    
                } else {
                $hour = [9, 10, 11, 13, 14, 15, 16, 17];
                if ($days[$day] == 6) {
                    $hour = array_merge($hour, [12, 18]);
                    sort($hour);
                }
            }
            
            ?>
                <form action="" method="post">
                    <div class="form-group">
                        <select name="hour">
                            <?php 
                              $real_hour = [];
                              foreach ($bookings_hour as $booking_hour) {
                                  array_push($real_hour,$booking_hour[0] );
                              }                  
    
                            foreach ($hour as $h) {
                              
                            if (!in_array($h, $real_hour)){
                               
                                echo '
                    <option  name="hour" value="' . $h . ' ">' . $h . ' : 00  </option>
                    ';
                          } else {
                             

                        }
                    }; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nom">Nom : </label>
                        <input id="nom" type="text" name="nom" size="45" placeholder="Nom">
                    </div>
                    <div class="form-group">
                        <label for="prenom">Prenom : </label>
                        <input id="prenom" type="text" name="prenom" size="45" placeholder="Prenom">
                    </div>
                    <div class="form-group">
                        <label for="tel">Telephone : </label>
                        <input id="tel" type="tel" name="tel" placeholder="Telephone">
                    </div>
                    <div class="form-group">
                        <label for="mail">Email : </label>
                        <input id="mail" type="mail" name="mail" placeholder="Mail">
                    </div>
                    <input type="hidden" name="information">
                    <input type="hidden" name="day" value=" <?php echo $_POST['choix']; ?>">

                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </form>
            <?php } ?>
        </div>

    </div>


    <!-- Extension jquery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <!-- Extension DATEPICKER -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <!-- Noyau JavaScript de Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Script pour activation du datepicker -->
    <script src="script.js"></script>


</body>

</html>